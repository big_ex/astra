FROM ubuntu:18.04

WORKDIR /usr/src

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Tashkent

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime &&  \
    echo $TZ > /etc/timezone && \
    apt-get update && \
    apt-get install -y build-essential yasm libssl-dev
